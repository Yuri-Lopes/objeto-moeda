  //const coin = {
    //state: 0,
    //flip: function() {
        // 1. Um ponto: Randomicamente configura a propriedade “estado” do 
        // seu objeto moeda para ser um dos seguintes valores:
        // 0 ou 1: use "this.state" para acessar a propriedade "state" neste objeto.
    //},
const coin = {
    state: 0,
    flip: function() {
      return (this.state = Math.floor(Math.random() * 2));
    },


   // toString: function() {
        // 2. Um ponto: Retorna a string "Heads" ou "Tails", dependendo de como
        //  "this.state" está como 0 ou 1.
   // },

    toString: function() {
      if (this.flip() == 0) {
        return 'Cara';
      } else {
        return 'Coroa';
      }
    },
    //toHTML: function() {
       // const image = document.createElement('img');
        // 3. Um ponto: Configura as propriedades do elemento imagem 
        // para mostrar a face voltada para cima ou para baixo dependendo
        // do valor de this.state está 0 ou 1.
      //  return image;
    
    toHTML: function() {
      const image = document.createElement('img');
      if (this.toString() == 'Cara') {
        image.src = 'img/Cara.jpg';
        document.body.appendChild(image);
      } else {
        image.src = 'img/Coroa.jpg';
        document.body.appendChild(image);
      }
      return image;
    }
  };



  //function display20Flips() {
    //const results = [];
    // 4. Um ponto: Use um loop para arremessar a moeda 20 vezes, cada vez 
    // mostrando o resultado como uma string na página. 
    // Depois de que seu loop terminar, retorne um array com o 
    // resultado de cada arremesso.
 
 //}
 
  function display20Flips() {
    const results = [];
    const mostrarResultados = document.createElement('p');
    for (let i = 0; i < 20; i++) {
      results.push(coin.toString());
    }
    mostrarResultados.innerText = results;
    document.body.appendChild(mostrarResultados);
    return results;
  }
  display20Flips();
  
  //function display20Images() {
    //const results = [];
    // 5. Um ponto: Use um loop para arremessar a moeda 20 vezes, cada vez 
    // mostrando o resultado como uma imagem na página. 
    // Depois de que seu loop terminar, retorne um array com o 
    // resultado de cada arremesso.
 
 //}


  function display20Images() {
    const results = [];
    for (let i = 0; i < 20; i++) {
      results.push(coin.toHTML());
    }
    return results;
  }
  display20Images();
  